#!/bin/zsh

make -f Makefile_linux

if [[ $? -eq 0 ]]; then
  ./wordcount_linux wordcount_linux.c Makefile_linux
fi

exit 0
