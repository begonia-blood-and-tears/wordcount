# wordcount

#### 记错了？
pthread_cond_wait 用在消费者线程里的时候原来还可以只调用一次（是指不需要检查等待的变量非空吗？）。

#### linux man pthread_cond_wait
Upon successful return, the mutex shall have been locked and shall be owned by the calling thread. If mutex is a robust mutex where an owner terminated while holding the lock and the state is recoverable, the mutex shall be acquired even though the function returns an error code. Windows 的 SleepConditionVariableCS 没有退出会自动锁住 CriticalSection 的说明。


#### 参考
- [SleepConditionVariableCS ](https://docs.microsoft.com/en-us/windows/win32/api/synchapi/nf-synchapi-sleepconditionvariablecs)
- man pthread_cond_wait
   - [linux](https://linux.die.net/man/3/pthread_cond_wait)
   - [OpenBSD](https://man.openbsd.org/pthread_cond_wait#:~:text=The%20pthread_cond_wait%20%28%29%20function%20atomically%20blocks%20the%20current,cond%2C%20and%20unblocks%20the%20mutex%20specified%20by%20mutex.)

